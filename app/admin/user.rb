ActiveAdmin.register User do

  index do
    selectable_column
    id_column
    column :email
    column :admin
    actions
  end
  
   form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :admin
    end
    f.actions
  end

  controller do
     def permitted_params
      params.permit user: [:email, :password, :password_confirmation,:admin]
    end

  end
end
