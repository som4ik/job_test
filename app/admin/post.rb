ActiveAdmin.register Post do

  permit_params :title, :description, :created_at

  index do
    selectable_column
    id_column
    column :Title
    column :description
    column :created_at
    actions
  end

end
