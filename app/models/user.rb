class User < ActiveRecord::Base
  has_and_belongs_to_many :categories
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         after_create :welcome_message
  private
  def welcome_message
   UserMailer.welcome_email(self).deliver
  end
#private

 # def welcome_message
  #  UserMailer.welcome_email(self).deliver
  #end
end
