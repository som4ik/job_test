class Post < ActiveRecord::Base
has_many :comments, :as => :commentable, :dependent => :destroy
has_and_belongs_to_many :categories
validates :title, presence: true, length: { minimum: 5 }

end
