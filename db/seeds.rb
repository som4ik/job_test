[
  { name: 'Politics'},
  { name: 'Sport'},
  { name: 'Tehnology'},
  { name: 'Music'},
  { name: 'Business'},
  { name: 'Other'}
].each do |category_attributes|
  category = Category.find_or_initialize_by(name: category_attributes[:name])
  category.update(category_attributes)
end
